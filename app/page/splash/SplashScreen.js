import React, {Component} from 'react';
import {View} from 'native-base';
import {Image} from 'react-native';
import UserApi from '../../data/network/UserApi';
import MyPref from '../../tools/MyPref';
import {Toast} from 'native-base';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    MyPref.getItem('is_login', value => {
      console.log('Have you logged in ?', value);
      if (value != null && value === true) {
        MyPref.getItem('user', user => {
          this.setState({
            username: user.username,
            password: user.password,
          });
          this.login();
        });
      }
    });
  }

  login() {
    UserApi.login(
      {
        username: this.state.username,
        password: this.state.password,
      },
      response => {
        if (response.success) {
          console.log('Response', response);
          if (response.data.access_token != null) {
            this.askChannel(response.data.access_token);

            // save into database if login is success
            MyPref.setItem('user', {
              username: this.state.username,
              password: this.state.password,
            });
            MyPref.setItem('is_login', true);
            MyPref.setItem('access_token', response.data.access_token);
          } else {
            Toast.show({
              text: 'Username atau Password Salah!',
              buttonText: 'Okay',
              type: 'danger',
            });
          }
        } else {
          alert('System error');
        }
      },
    );
    return true;
  }

  askChannel(token) {
    const {navigate} = this.props.navigation;
    UserApi.requestChannel(token, response => {
      if (response.success) {
        navigate('Home', response.data);
        MyPref.setItem('channel', response.data);
      }
    });
  }

  async componentDidMount() {
    await this.sleep(3000);
    this.props.navigation.replace('Login');
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  render() {
    return (
      <View style={{justifyContent: 'center', flex: 1}}>
        <Image
          style={this.style.logo}
          source={require('../../assets/image/via.png')}
          resizeMode="contain"
        />
      </View>
    );
  }

  style = {
    logo: {
      alignSelf: 'center',
      width: 300,
      flex: 1,
    },
  };
}
