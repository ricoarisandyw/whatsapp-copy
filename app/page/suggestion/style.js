import {Color} from '../../tools/MyStyle';

export const bubbleWrapperStyle = {
  left: {
    borderBottomColor: 'silver',
    borderBottomWidth: 2,
    borderRightColor: 'silver',
    borderRightWidth: 1,
  },
  right: {
    backgroundColor: Color.BUBBLE_GREEN,
    borderBottomColor: 'silver',
    borderBottomWidth: 2,
    borderLeftColor: 'silver',
    borderLeftWidth: 1,
  },
};
