import React, {useRef} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import SuggestionApi from '../../data/network/SuggestionApi';
import {
  Header,
  Body,
  Container,
  Left,
  Right,
  Title,
  Button,
  Icon,
  Tab,
  Tabs,
  TabHeading,
  Accordion,
} from 'native-base';
import Video from 'react-native-video';
import {Bubble, MessageText, GiftedChat} from 'react-native-gifted-chat';
import ChatroomApi from '../../data/network/ChatroomApi';
import ChatTemplate from '../chats/ChatTemplate';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Color} from '../../tools/MyStyle';

import {bubbleWrapperStyle} from './style';

class SuggestionScreen extends React.Component {
  state = {
    suggestion: [],
    data: [],
    chat: {},
    answer: [],
    answer_knowledge: [],
    knowledge: [],
    raw_knowledge: [],
    selected_intent: {},
    selected_suggestion: {},
    messages: [],
    messages_knowledge: [],
  };

  constructor(props) {
    super(props);
    this.loadAnswer = this.loadAnswer.bind(this);
    this.loadIntent = this.loadIntent.bind(this);
    this.renderAccordionContent = this.renderAccordionContent.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.childFinder = this.childFinder.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.sendTrainMessage = this.sendTrainMessage.bind(this);

    this.state.chat = props.navigation.state.params.chat;

    this.initData();
  }

  async initData() {
    await SuggestionApi.getSuggestionById(this.state.chat.id, json => {
      this.setState({
        data: json,
      });
    });

    await SuggestionApi.getKnowledge(list_knowledge => {
      this.setState({
        raw_knowledge: list_knowledge,
      });
      //Convert
      var grand_parent = list_knowledge.filter(
        value => value.parentId === 0,
      )[0];
      grand_parent.childs = this.childFinder(grand_parent.theId);
      this.setState({
        knowledge: [grand_parent],
      });
    });
  }

  render() {
    var it = this;
    const {
      knowledge,
      data,
      messages,
      selected_suggestion,
      selected_intent,
      chat,
      messages_knowledge,
    } = this.state;

    return (
      <Container>
        <Header
          androidStatusBarColor={Color.BACKGROUND_DARK}
          style={{height: 60, backgroundColor: Color.BACKGROUND}}>
          <Left
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <Icon style={{color: Color.FONT}} name="md-arrow-back" />
            </Button>
            <Title style={{color: Color.FONT, fontSize: 18, marginLeft: 20}}>
              Suggestion
            </Title>
          </Left>
          <Body style={{marginLeft: -20}} />
        </Header>
        <Tabs>
          <Tab
            heading={
              <TabHeading
                style={{
                  justifyContent: 'center',
                  backgroundColor: Color.BACKGROUND,
                }}>
                {/* <Icon name="camera" /> */}
                <Text
                  style={{
                    color: Color.FONT,
                  }}>
                  Suggestion
                </Text>
              </TabHeading>
            }>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{flex: 1}}>
                {data.length > 0 ? (
                  data.map(value => {
                    return this.renderSuggestion(value);
                  })
                ) : (
                  <Text>Loading . . .</Text>
                )}
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  backgroundColor: 'white',
                  margin: 10,
                  flex: 1,
                }}>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                  Saran Jawaban
                </Text>
                <GiftedChat
                  inverted={true}
                  messages={messages}
                  renderBubble={this.renderBubble}
                  renderComposer={props =>
                    it.state.answer.length < 1 ? (
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <Text>Pilih sugesti diatas</Text>
                      </View>
                    ) : (
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <this.MyButton
                          text="TRAIN"
                          onPress={() => {
                            this.loadAnswer({
                              ...selected_suggestion,
                              question: chat.message,
                              realChat: true,
                              data: [
                                {
                                  name: 'name',
                                  secret: false,
                                  value: this.props.selected_chat.initialuser
                                    .name,
                                },
                              ],
                            });
                            this.sendTrainMessage(messages.reverse());
                          }}
                        />
                        <this.MyButton
                          text="SEND"
                          onPress={() => {
                            this.sendMessage(messages.reverse());
                          }}
                        />
                      </View>
                    )
                  }
                  user={{
                    _id: 1,
                  }}
                />
              </View>
            </View>
          </Tab>
          <Tab
            heading={
              <TabHeading
                style={{
                  justifyContent: 'center',
                  backgroundColor: Color.BACKGROUND,
                }}>
                {/* <Icon name="camera" /> */}
                <Text
                  style={{
                    color: Color.FONT,
                  }}>
                  Knowledge
                </Text>
              </TabHeading>
            }>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{flex: 1}}>
                <Accordion
                  dataArray={knowledge}
                  expanded={0}
                  renderHeader={this.renderHeader}
                  renderContent={this.renderAccordionContent}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  backgroundColor: 'white',
                  margin: 10,
                  flex: 1,
                }}>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                  Isi Knowledge
                </Text>
                <GiftedChat
                  inverted={true}
                  style
                  messages={messages_knowledge}
                  renderComposer={props =>
                    it.state.answer_knowledge.length < 1 ? (
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <Text>Pilih sugesti diatas</Text>
                      </View>
                    ) : (
                      <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <this.MyButton
                          text="TRAIN"
                          onPress={() => {
                            this.loadAnswer({
                              ...selected_intent,
                              realChat: true,
                            });
                            this.sendTrainMessage(messages_knowledge.reverse());
                          }}
                        />
                        <this.MyButton
                          text="SEND"
                          onPress={() => {
                            this.sendMessage(messages_knowledge.reverse());
                          }}
                        />
                      </View>
                    )
                  }
                  renderBubble={this.renderBubble}
                  user={{
                    _id: 1,
                  }}
                />
              </View>
            </View>
          </Tab>
        </Tabs>
      </Container>
    );
  }

  renderBubble = props => {
    const it = this;
    if (props.currentMessage.messagetype === 'video') {
      return (
        <Bubble
          {...props}
          wrapperStyle={bubbleWrapperStyle}
          renderMessageText={props => {
            var {currentMessage} = props;
            return (
              <View>
                <MessageText {...props} />
                <TouchableOpacity
                  onPress={() => {
                    it.goTo('Video', currentMessage);
                  }}>
                  <View style={{padding: 10}}>
                    <Video
                      paused={true}
                      // fullscreen={true}
                      style={{width: 200, height: 200}}
                      source={{
                        uri: props.currentMessage.video_url,
                      }} // Can be a URL or a local file.
                      ref={ref => {
                        this.player = ref;
                      }} // Store reference
                      onBuffer={this.onBuffer} // Callback when remote video is buffering
                      onError={this.videoError} // Callback when video cannot be loaded
                      onClick={() => {
                        alert('Clicked');
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            );
          }}
        />
      );
    } else if (props.currentMessage.messagetype === 'audio') {
      return ChatTemplate.Audio({
        ...props,
        wrapperStyle: bubbleWrapperStyle,
      });
    } else if (props.currentMessage.messagetype === 'template') {
      return ChatTemplate.Carousel({
        ...props,
        wrapperStyle: bubbleWrapperStyle,
      });
      // } else if (props.currentMessage.messagetype === 'image'){
      //   return ChatTemplate.Image
    } else {
      return ChatTemplate.Text({
        ...props,
        wrapperStyle: it.bubbleWrapperStyle,
      });
    }
  };

  sendMessage(messages) {
    var messages_with_data = messages[0];
    messages_with_data = {
      ...messages_with_data,
      data: messages[0].raw.body,
    };

    // Send message
    ChatroomApi.sendCustomChat(
      {
        body: messages_with_data,
        id_room: this.props.selected_chat.id,
      },
      response => {
        if (response.success) {
          if (messages.length > 1) {
            this.sendMessage(messages.slice(1, messages.length));
          } else {
            this.props.navigation.goBack();
          }
        } else {
          console.error('gagal mengirim pesan dari react');
        }
      },
    );
  }

  sendTrainMessage(messages) {
    var train_message = messages[0];
    train_message = {
      ...train_message,
      info: {
        id: this.state.selected_suggestion.response,
        faqId: this.state.selected_suggestion.faqid,
        question: this.state.chat.message,
      },
    };

    // Send message
    ChatroomApi.sendCustomChat(
      {
        body: train_message,
        id_room: this.props.selected_chat.id,
        source: 'rooc:composer:train',
        rooc_token: this.props.user.access_token,
      },
      response => {
        if (response.success) {
          if (messages.length > 1) {
            this.sendTrainMessage(messages.slice(1, messages.length));
          } else {
            this.props.navigation.goBack();
          }
        } else {
          console.error('gagal mengirim pesan dari react');
        }
      },
    );
  }

  renderHeader(item, expanded) {
    if (item.childs == null) {
      return (
        <TouchableOpacity
          onPress={() => {
            // console.warn('Item', );
            this.loadIntent({
              faqid: item.theId,
              question: this.state.chat.message,
              response: this.state.chat.info.responseid,
              realChat: false,
            });
          }}>
          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              justifyContent: 'space-between',
              alignItems: 'center',
              borderWidth: 1,
              borderColor: 'silver',
            }}>
            <Text style={{fontWeight: '600'}}> {item.title}</Text>
            <Icon style={{fontSize: 12}} name="ios-radio-button-on" />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View
          style={{
            flexDirection: 'row',
            padding: 10,
            justifyContent: 'space-between',
            alignItems: 'center',
            borderWidth: 1,
            borderColor: 'silver',
          }}>
          <Text style={{fontWeight: '600'}}> {item.title}</Text>
          {expanded ? (
            <Icon style={{fontSize: 18}} name="remove-circle" />
          ) : (
            <Icon style={{fontSize: 18}} name="add-circle" />
          )}
        </View>
      );
    }
  }

  renderAccordionContent(item) {
    if (item.childs == null) {
      return (
        <View>
          <Text
            style={{
              backgroundColor: '#e3f1f1',
              padding: 10,
              fontStyle: 'italic',
            }}>
            {JSON.stringify(this.state.selected_intent)}
          </Text>
        </View>
      );
    } else {
      return (
        <Accordion
          style={{borderLeftColor: 'white', borderLeftWidth: 20}}
          dataArray={item.childs}
          renderHeader={this.renderHeader}
          renderContent={this.renderAccordionContent}
        />
      );
    }
  }

  MyButton = props => (
    <Button
      style={{
        alignSelf: 'center',
        padding: 10,
        marginRight: 10,
        backgroundColor: Color.BUBBLE_GREEN,
      }}
      onPress={props.onPress}>
      <Text>{props.text}</Text>
    </Button>
  );

  MyMenu(props) {
    const menu = useRef();

    const hideMenu = () => {
      menu.current.hide();
      console.warn(props.suggestion);
      //add back function
      props.currentMessage.raw.onGoBack = this.onSend;
      props.suggestion('Suggestion', props.currentMessage.raw);
    };

    const replyMenu = () => {
      menu.current.hide();
      props.reply(props.currentMessage);
    };

    const showMenu = () => menu.current.show();
    // return props.child;
    return (
      <View>
        {props.child(showMenu)}
        <Menu ref={menu}>
          <MenuItem onPress={replyMenu}>Reply</MenuItem>
          <MenuItem onPress={hideMenu}>Sugestion</MenuItem>
        </Menu>
      </View>
    );
  }

  renderSuggestion(suggestion) {
    console.warn('Suggestion', suggestion);
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({
            selected_suggestion: suggestion,
          });
          this.loadAnswer(suggestion);
        }}>
        <View
          style={{
            padding: 20,
            borderBottomColor: 'silver',
            borderBottomWidth: 1,
            flexDirection: 'row',
          }}>
          <View style={{flexDirection: 'column'}}>
            <Text style={{fontSize: 12, fontWeight: 'bold'}}>
              {suggestion.faqid}/{suggestion.question}
            </Text>
            <Text style={{fontSize: 10}}>{suggestion.answer}</Text>
            <Text style={{fontSize: 10}}>
              {suggestion.confidence.toFixed(2)} %
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  loadAnswer(suggestion) {
    SuggestionApi.getAnswerBySuggestion(
      {suggestion, rooc_token: this.props.user.access_token},
      response => {
        var convertedResponse = response
          .map(value => this.convertChatToBubble(value))
          .reverse();
        this.setState({
          answer: response,
          messages: convertedResponse,
        });
      },
    );
  }

  loadIntent(suggestion) {
    SuggestionApi.getAnswerBySuggestion(
      {suggestion, rooc_token: this.props.user.access_token},
      response => {
        var convertedResponse = response
          .map(value => this.convertChatToBubble(value))
          .reverse();
        this.setState({
          answer_knowledge: response,
          messages_knowledge: convertedResponse,
        });
      },
    );
  }

  childFinder(parent_id) {
    //Get list child
    var childs = this.state.raw_knowledge.filter(
      value => value.parentId === parent_id,
    );

    //For each child, get list child
    var full_child = childs.map(value => {
      var will_child = this.childFinder(value.theId);
      if (will_child.length > 0) value.childs = will_child;
      return value;
    });
    return full_child;
  }

  convertChatToBubble(value) {
    value.messagetype = value.type;

    return {
      _id: 1,
      raw: value,
      createdAt: value.time,
      text: value.messagetype === 'text' ? value.body.text : ' ',
      messagetype: value.messagetype,
      message:
        value.messagetype == 'text'
          ? value.body.text
          : value.messagetype == 'image'
          ? value.url
          : null,
      user: {
        _id: 1,
        name: '',
        avatar: '',
      },
      carousel:
        value.messagetype === 'template'
          ? value.body.template.columns.map(data => {
              return {
                title: data.text,
                subtitle: data.defaultAction.text,
                illustration: data.thumbnailImageUrl,
              };
            })
          : [],
      video_url: value.messagetype === 'video' ? value.attachments : '',
      image: value.messagetype === 'image' ? value.url : null,
    };
  }

  check_usertype = usertype => {
    if (usertype === 'bot' || usertype === 'sysadmin') {
      return 1;
    } else {
      return 2;
    }
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

const mapStateToProps = state => {
  const {selected_chat, selected_bubble, user} = state;
  return {
    selected_chat,
    selected_bubble,
    user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SuggestionScreen);
