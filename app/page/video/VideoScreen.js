import React, {useRef} from 'react';
import Video from 'react-native-video';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {View} from 'react-native';

import {styleVideo, styleVideoFrame} from './style';

class VideoScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const messageData = this.props.navigation.state.params.chat;
    return (
      <View style={styleVideoFrame}>
        <Video
          controls={true}
          style={styleVideo}
          source={{uri: messageData.video_url}} // Can be a URL or a local file.
          ref={ref => {
            this.player = ref;
          }} // Store reference
          onBuffer={this.onBuffer} // Callback when remote video is buffering
          onError={this.videoError} // Callback when video cannot be loaded
        />
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

const mapStateToProps = state => {
  const {selected_bubble} = state;
  return {
    selected_bubble,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VideoScreen);
