export const styleVideoFrame = {
  flex: 1,
  flexDirection: 'row',
  padding: 10,
  backgroundColor: 'black',
  justifyContent: 'space-between',
};

export const styleVideo = {
  width: '100%',
  height: 400,
  alignSelf: 'center',
};
