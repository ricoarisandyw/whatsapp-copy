import React, {useRef} from 'react';
import {
  Text,
  Image,
  View,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {
  GiftedChat,
  Bubble,
  MessageText,
  Composer,
} from 'react-native-gifted-chat';
import {
  Header,
  Body,
  Container,
  Left,
  Right,
  Title,
  Button,
  Icon,
} from 'native-base';
import SoundPlayer from 'react-native-sound-player';
import Carousel from 'react-native-snap-carousel';
import {sliderWidth, itemWidth} from './SliderEntry.style';
import SliderEntry from './SliderEntry';
import ChatTemplate from './ChatTemplate';
import Slider from '@react-native-community/slider';
import Video from 'react-native-video';
import SockJsClient from 'react-stomp';
import {STOMP} from '../../data/network/StompConst';
import MyPref from '../../tools/MyPref';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import {Color} from '../../tools/MyStyle';
import {Linking} from 'react-native';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import ChatroomApi from '../../data/network/ChatroomApi';

import {addDummy} from '../../data/redux/actions/index';

class ChatPage extends React.Component {
  state = {
    messages: [],
    raw_messages: [],
    input_text: '',
    reply_text: null,
  };

  constructor(props) {
    super(props);
    this.check_usertype = this.check_usertype.bind(this);
    this.refreshChatRoom = this.refreshChatRoom.bind(this);
    this.onMessageReceive = this.onMessageReceive.bind(this);
    this.goTo = this.goTo.bind(this);
    this.MyMenu = this.MyMenu.bind(this);
    this.onSend = this.onSend.bind(this);
    this.renderComposer = this.renderComposer.bind(this);
    this.leaveRoom = this.leaveRoom.bind(this);

    //Load channel from SharedPreferences
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      this.setState({
        channel: channel,
      });
      this.refreshChatRoom();
    });
  }

  goTo(target, value) {
    console.warn('Go To', target, value);
    this.props.navigation.navigate(target, {
      chat: value,
    });
  }

  check_usertype = usertype => {
    if (usertype === 'bot' || usertype === 'sysadmin') {
      return 1;
    } else {
      return 2;
    }
  };

  onReply = message => {
    this.setState({
      reply_text: message.text,
    });
  };

  cancelReply = () => {
    this.setState({
      reply_text: null,
    });
  };

  refreshChatRoom() {
    // Load Chatroom
    ChatroomApi.openChatroom(
      {
        id: this.props.selected_chat.id,
      },
      response_json => {
        if (response_json.success) {
          this.setState({
            raw_messages: response_json.data,
          });
          //Parsing here
          this.setState({
            messages: this.state.raw_messages.map((value, index) => {
              if (value.status === 'fail') {
                return {
                  _id: 2,
                  createdAt: value.time,
                  text: value.message,
                  messagetype: 'fail',
                  user: {
                    _id: this.check_usertype(value.sender.usertype),
                    name: 'fail',
                    // avatar: 'https://placeimg.com/100/100/people',
                    avatar: this.props.selected_chat.initialuser.avatar,
                  },
                };
              } else {
                return {
                  _id: index,
                  raw: value,
                  createdAt: value.time,
                  text:
                    (value.messagetype === 'text' ? value.message : ' ') +
                    (parseFloat(value.confidence) < 100 &&
                    this.check_usertype(value.sender.usertype) != 1
                      ? ' (' + value.confidence + '%) '
                      : ''),
                  messagetype:
                    value.messagetype === 'template'
                      ? value.data.template.type
                      : value.messagetype,
                  user: {
                    _id: this.check_usertype(value.sender.usertype),
                    name:
                      value.sender.usertype == 'bot' ||
                      value.sender.usertype == 'sysadmin'
                        ? value.sender.usertype
                        : value.sender.firstname,
                    // avatar: 'https://placeimg.com/100/100/people',
                    avatar: this.props.selected_chat.initialuser.avatar,
                  },
                  carousel:
                    value.messagetype === 'template' &&
                    value.data.template.type === 'carousel'
                      ? value.data.template.columns.map(data => {
                          console.log(data);
                          return {
                            title: data.text,
                            subtitle:
                              data.defaultAction == null
                                ? data.action.text
                                : data.defaultAction.text,
                            illustration:
                              data.thumbnailImageUrl || data.imageUrl,
                          };
                        })
                      : [],
                  buttons: {
                    actions:
                      value.messagetype === 'template' &&
                      value.data.template.type === 'buttons'
                        ? value.data.template.actions
                        : [],
                    thumbnailImageUrl:
                      value.messagetype === 'template' &&
                      value.data.template.type === 'buttons'
                        ? value.data.template.thumbnailImageUrl
                        : '',
                  },
                  video_url:
                    value.messagetype === 'video' ? value.attachments : '',
                  // video_url:
                  //   'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
                  image: value.messagetype === 'image' ? value.message : null,
                  audio:
                    value.messagetype === 'audio' ? value.attachments : null,
                };
              }
            }),
          });
        }
      },
    );
  }

  componentDidMount() {}

  onMessageReceive(msg, topic) {
    if (topic === STOMP.CHAT_ROOM + this.props.selected_chat.id) {
      this.refreshChatRoom();
    }
  }

  onSend(messages = []) {
    // Send message
    ChatroomApi.sendChat(
      {
        text: messages[0].text,
        id_room: this.props.selected_chat.id,
      },
      response => {
        if (response.success) {
          // this.setState(previousState => ({
          //   messages: GiftedChat.append(previousState.messages, messages),
          // }));
          if (messages.length > 1) {
            this.onSend(messages.slice(1, messages.length));
          }
        } else {
          console.error('gagal mengirim pesan dari react');
        }
      },
    );
  }

  render() {
    const it = this;
    const {state} = this.props.navigation;
    const contact = state.params;
    return (
      <Container>
        <Header
          androidStatusBarColor={Color.BACKGROUND_DARK}
          style={{
            height: 60,
            backgroundColor: Color.BACKGROUND,
            borderBottomColor: Color.BACKGROUND_ACCENT,
            borderBottomWidth: 0.5,
          }}>
          <Left
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack(null);
              }}>
              <Icon name="md-arrow-back" style={{color: Color.FONT}} />
            </Button>
            <Image
              source={{uri: this.props.selected_chat.initialuser.avatar}}
              style={{
                height: 40,
                width: 40,
                borderRadius: 50,
              }}
            />
            <Text
              style={{
                color: Color.FONT,
                fontSize: 18,
                marginLeft: 20,
                width: 200,
              }}>
              {contact.chat.subject}
            </Text>
          </Left>
          <Body style={{marginLeft: -20}} />
          <Right>
            <this.ChatMenu
              onLeave={this.leaveRoom}
              child={onLongPress => {
                return (
                  <Button transparent onPress={onLongPress}>
                    <Icon name="md-more" style={{color: Color.FONT}} />
                  </Button>
                );
              }}
            />
          </Right>
        </Header>
        <ImageBackground
          source={require('../../assets/image/bg_whatsapp.png')}
          style={{flex: 1}}>
          <SockJsClient
            url={'https://channel.rooc.egeroo.com/rooc'}
            headers={{
              Authorization: 'Bearer ' + this.props.channel.userchnltoken,
            }}
            topics={[STOMP.CHAT_ROOM + this.props.selected_chat.id]}
            onMessage={this.onMessageReceive}
            onConnect={() => {
              console.warn('Sokcet connected');
              this.setState({clientConnected: true});
            }}
            onDisconnect={() => {
              console.warn('Socket disconnected');
              this.setState({clientConnected: false});
            }}
            autoReconnect={true}
            getRetryInterval={() => {
              return 3000;
            }}
            debug={false}
            ref={client => {
              this.clientRef = client;
            }}
          />
          <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            parsePatterns={linkStyle => [
              {
                type: 'phone',
                style: LinkStyle,
                onPress: phone_number => {
                  Linking.openURL(`tel:${phone_number}`);
                },
              },
              {
                type: 'url',
                style: LinkStyle,
                onPress: url => {
                  Linking.openURL(url);
                },
              },
              {
                type: 'email',
                style: LinkStyle,
                onPress: email => {
                  Linking.openURL(`mailto:${email}`);
                },
              },
            ]}
            renderComposer={this.renderComposer}
            renderCustomView={props => {}}
            renderBubble={props => {
              if (props.currentMessage.messagetype === 'video') {
                return (
                  <Bubble
                    {...props}
                    wrapperStyle={this.bubbleWrapperStyle}
                    renderMessageText={props => {
                      var {currentMessage} = props;
                      return (
                        <View>
                          <MessageText {...props} />
                          <TouchableOpacity
                            onPress={() => {
                              it.goTo('Video', currentMessage);
                            }}>
                            <View style={{padding: 10}}>
                              <Video
                                paused={true}
                                // fullscreen={true}
                                style={{width: 200, height: 200}}
                                source={{uri: props.currentMessage.video_url}} // Can be a URL or a local file.
                                ref={ref => {
                                  this.player = ref;
                                }} // Store reference
                                onBuffer={this.onBuffer} // Callback when remote video is buffering
                                onError={this.videoError} // Callback when video cannot be loaded
                                onClick={() => {
                                  alert('Clicked');
                                }}
                              />
                            </View>
                          </TouchableOpacity>
                        </View>
                      );
                    }}
                  />
                );
              } else if (props.currentMessage.messagetype === 'audio') {
                return ChatTemplate.Audio({
                  ...props,
                  wrapperStyle: this.bubbleWrapperStyle,
                });
              } else if (props.currentMessage.messagetype === 'carousel') {
                return ChatTemplate.Carousel({
                  ...props,
                  wrapperStyle: this.bubbleWrapperStyle,
                });
              } else if (props.currentMessage.messagetype === 'buttons') {
                return ChatTemplate.Button({
                  ...props,
                  wrapperStyle: it.bubbleWrapperStyle,
                });
              } else {
                return (
                  <this.MyMenu
                    {...props}
                    suggestion={it.goTo}
                    reply={it.onReply}
                    cancelReply={it.cancelReply}
                    child={onLongPress => {
                      return ChatTemplate.Text({
                        ...props,
                        onLongPress: onLongPress,
                        wrapperStyle: it.bubbleWrapperStyle,
                      });
                    }}
                  />
                );
              }
            }}
            user={{
              _id: 1,
            }}
          />
        </ImageBackground>
      </Container>
    );
  }

  ChatMenu(props) {
    const menu = useRef();

    const hideMenu = () => {
      menu.current.hide();
      console.warn(props.suggestion);
      //add back function
      props.suggestion('Suggestion', props.currentMessage.raw);
    };

    const replyMenu = () => {
      menu.current.hide();
      props.reply(props.currentMessage);
    };

    const showMenu = () => menu.current.show();

    return (
      <View>
        {props.child(showMenu)}
        <Menu ref={menu}>
          <MenuItem onPress={props.onEscalate}>Escalate</MenuItem>
          <MenuItem onPress={props.onAssign}>Assign</MenuItem>
          <MenuItem onPress={props.onLeave}>Leave</MenuItem>
        </Menu>
      </View>
    );
  }

  MyMenu(props) {
    const menu = useRef();

    const hideMenu = () => {
      menu.current.hide();
      console.warn(props.suggestion);
      //add back function
      props.currentMessage.raw.onGoBack = this.onSend;
      props.suggestion('Suggestion', props.currentMessage.raw);
    };

    const replyMenu = () => {
      menu.current.hide();
      props.reply(props.currentMessage);
    };

    const showMenu = () => menu.current.show();
    // return props.child;
    return (
      <View>
        {props.child(showMenu)}
        <Menu ref={menu}>
          <MenuItem onPress={replyMenu}>Reply</MenuItem>
          <MenuItem onPress={hideMenu}>Sugestion</MenuItem>
        </Menu>
      </View>
    );
  }

  leaveRoom() {
    ChatroomApi.leaveRoom(this.props.selected_chat.id, callback => {
      this.props.navigation.goBack(null);
    });
  }

  renderComposer = props => {
    return (
      <View
        style={{
          flexDirection: 'column',
          width: '100%',
          backgroundColor: Color.BACKGROUND,
        }}>
        {this.state.reply_text != null ? (
          <View style={{padding: 10, flexDirection: 'row'}}>
            <Text style={{flex: 1}}>{this.state.reply_text}</Text>
            <Icon
              name="md-close"
              onPress={() => {
                this.setState({
                  reply_text: null,
                });
              }}
            />
          </View>
        ) : null}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <Composer
            {...props}
            text={this.state.input_text}
            style={{
              flex: 1,
            }}
            onTextChanged={text => {
              this.setState({
                input_text: text,
              });
              props.onTextChanged(text);
            }}
          />
          {this.state.input_text.length > 0 ? (
            <Icon
              onPress={() => {
                this.setState({
                  input_text: '',
                });
                this.onSend([
                  {
                    text: this.state.input_text,
                  },
                ]);
              }}
              style={{width: 40, color: Color.FONT, alignSelf: 'center'}}
              name="md-paper-plane"
            />
          ) : null}
          <Icon
            onPress={() => {
              const sender_messages = this.state.messages
                .filter(message => {
                  return message.user._id == 2;
                })
                .reverse();
              if (sender_messages.length == 0) {
                alert('Tidak ada pesan terakhir dari client');
              } else {
                var last_messages =
                  sender_messages[sender_messages.length - 1].raw;
                last_messages.onGoBack = this.onSend;
                if (last_messages != 0) {
                  this.goTo('Suggestion', last_messages);
                }
              }
            }}
            style={{width: 40, color: Color.FONT, alignSelf: 'center'}}
            name="md-book"
          />
        </View>
      </View>
    );
  };

  bubbleWrapperStyle = {
    left: {
      borderBottomColor: 'silver',
      borderBottomWidth: 2,
      borderRightColor: 'silver',
      borderRightWidth: 1,
    },
    right: {
      backgroundColor: Color.BUBBLE_GREEN,
      borderBottomColor: 'silver',
      borderBottomWidth: 2,
      borderLeftColor: 'silver',
      borderLeftWidth: 1,
    },
  };
}

//=======STYLE==========//
const LinkStyle = {
  color: 'blue',
  textDecorationLine: 'underline',
  fontWeight: 'bolder',
};

//=======REDUX==========//
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addDummy,
    },
    dispatch,
  );
}

const mapStateToProps = state => {
  const {
    selected_chat,
    selected_bubble,
    selected_suggestion,
    selected_knowledge,
    chats,
    channel,
  } = state;
  return {
    selected_chat,
    selected_bubble,
    selected_suggestion,
    selected_knowledge,
    chats,
    channel,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatPage);
