class BotReplierHelper {
  reply(message) {
    if (message == '/image') {
      return {
        _id: 2,
        text: 'Here is image message',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
        image:
          'http://1ttnwj3qbvu23kfodm1zk0a9-wpengine.netdna-ssl.com/wp-content/uploads/pyrenees-aigues-tortes-national-park-hd-s.jpg',
        // additional custom parameters
      };
    } else if (message == 'video') {
      return {
        _id: 3,
        text: 'Here are the video message',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
        // You can also add a video prop:
        video:
          'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
        // Any additional custom parameters are passed through
      };
    } else {
      return {
        _id: 1,
        text: 'Hello developer',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
      };
    }
  }
}
