import React, {useRef} from 'react';
import {sliderWidth, itemWidth} from './SliderEntry.style';
import SliderEntry from './SliderEntry';
import Carousel from 'react-native-snap-carousel';
import Slider from '@react-native-community/slider';
import SoundPlayer from 'react-native-sound-player';
import Sound from 'react-native-sound';

import {
  Text,
  Image,
  View,
  ImageBackground,
  TouchableOpacity,
  Alert,
  StyleSheet,
} from 'react-native';
import {GiftedChat, Bubble, MessageText, Time} from 'react-native-gifted-chat';
import Video from 'react-native-video';
import {
  Header,
  Body,
  Container,
  Left,
  Right,
  Title,
  Button,
  Icon,
} from 'native-base';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import {Color} from '../../tools/MyStyle';
import {Linking} from 'react-native';

export default class ChatTemplate {
  /**
   * Carousel Bubble
   */
  static Carousel = props => {
    const carousel_style = {
      // backgroundColor: 'silver',
      shadowOffset: {width: 10, height: 10},
      shadowColor: 'black',
      shadowOpacity: 1.0,
      marginLeft: -50,
      paddingTop: 20,
    };

    return (
      <View style={carousel_style}>
        <Carousel
          layoutCardOffset={'0'}
          ref={c => {
            this._carousel = c;
          }}
          data={props.currentMessage.carousel}
          renderItem={this.renderCarouselItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
        />
      </View>
    );
  };

  static renderCarouselItem = ({item, index}) => {
    return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
  };

  // /**
  //  * Video Bubble
  //  */
  // static Video = props => {
  //   return (
  //     <Bubble
  //       {...props}
  //       renderMessageText={props => {
  //         return (
  //           <View>
  //             <MessageText {...props} />
  //             <View style={{padding: 10}}>
  //               <Video
  //                 style={{height: 150}}
  //                 source={{uri: props.currentMessage.video_url}} // Can be a URL or a local file.
  //                 ref={ref => {
  //                   parent.player = ref;
  //                 }} // Store reference
  //                 onBuffer={parent.onBuffer} // Callback when remote video is buffering
  //                 onError={parent.videoError} // Callback when video cannot be loaded
  //               />
  //             </View>
  //           </View>
  //         );
  //       }}
  //     />
  //   );
  // };

  /**
   * Text Bubble
   */
  static Text = props => {
    return (
      <Bubble
        {...props}
        renderMessageText={props => {
          return (
            <TouchableOpacity onPress={props.onLongPress}>
              <View>
                <Text
                  style={{marginLeft: 10, marginTop: 10, color: Color.FONT}}>
                  {props.currentMessage.user.name}
                </Text>
              </View>
              <MessageText {...props} />
            </TouchableOpacity>
          );
        }}
        textStyle={{
          right: {
            color: 'black',
            textDecorationColor: 'gray',
          },
        }}
        renderTime={props => {
          return (
            <Time
              {...props}
              timeTextStyle={{
                right: {
                  color: 'silver',
                },
                left: {
                  color: 'silver',
                },
              }}
              // timeFormat={'YYYY-MM-DD HH:mm:ss'}
            />
          );
        }}
      />
    );
  };

  static Image = props => {
    return (
      <View>
        <Image
          style={{width: 150, height: 150}}
          source={{uri: props.currentMessage.image}}
        />
      </View>
    );
  };

  static Button = props => {
    const buttonStyle = {
      margin: 10,
      marginTop: 5,
      backgroundColor: 'silver',
      padding: 5,
      textAlign: 'center',
    };

    const containerStyle = {
      margin: 10,
    };

    return (
      <Bubble
        {...props}
        renderMessageText={props => {
          return (
            <TouchableOpacity onPress={props.onLongPress}>
              <View>
                <Text
                  style={{marginLeft: 10, marginTop: 10, color: Color.FONT}}>
                  {props.currentMessage.user.name}
                </Text>
                <Image
                  style={{width: 150, height: 150, margin: 10}}
                  source={{uri: props.currentMessage.buttons.thumbnailImageUrl}}
                />
                {props.currentMessage.buttons.actions.map(action => (
                  <Button style={buttonStyle}>
                    <Text style={{textAlign: 'center'}}>{action.label}</Text>
                  </Button>
                ))}
              </View>
              <MessageText {...props} />
            </TouchableOpacity>
          );
        }}
        textStyle={{
          right: {
            color: 'black',
            textDecorationColor: 'gray',
          },
        }}
        renderTime={props => {
          return (
            <Time
              {...props}
              timeTextStyle={{
                right: {
                  color: 'silver',
                },
                left: {
                  color: 'silver',
                },
              }}
              // timeFormat={'YYYY-MM-DD HH:mm:ss'}
            />
          );
        }}
      />
    );
  };

  static Audio = props => {
    return (
      <Bubble
        {...props}
        renderMessageText={props => {
          return (
            <View>
              <MessageText {...props} />
              <View style={{padding: 10}}>{this._audioMessage(props)}</View>
            </View>
          );
        }}
      />
    );
  };

  static _audioMessage = props => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={async () => {
            const track = new Sound(props.currentMessage.audio, null, e => {
              if (e) {
                console.log('error loading track:', e);
              } else {
                track.play();
              }
            });
          }}>
          <Icon name="ios-play" />
        </TouchableOpacity>
        <Text>{'Press to Play Audio '}</Text>
        {/* <Slider
          style={{width: 200, height: 40}}
          minimumValue={0}
          maximumValue={1}
          // minimumTrackTintColor="#FFFFFF"
          maximumTrackTintColor="#000000"
        /> */}
      </View>
    );
  };
}
