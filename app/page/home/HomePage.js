import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Dimensions, AppState} from 'react-native';
import TabChats from './TabChats';
import {
  Container,
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Tabs,
  Tab,
  TabHeading,
  Badge,
} from 'native-base';
import S from '../../tools/MyStyle';
import MyBadge from '../../components/MyBadge';
import EnqueueApi from '../../data/network/EnqueueApi';
import HandleApi from '../../data/network/HandleApi';
import MyPref from '../../tools/MyPref';
import SockJsClient from 'react-stomp';
import {STOMP} from '../../data/network/StompConst';
import {Color} from '../../tools/MyStyle';
import PushNotification from 'react-native-push-notification';

//redux
import {selectChat} from '../../data/redux/actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      enqueue_list: [],
      handle_list: [],
      channel: {},
      clientConnected: false,
      appState: AppState.currentState,
      topics: [STOMP.NEW_QUEUE, STOMP.ROOM],
      id_messages: [],
      id_new_message: null,
    };

    this.refreshQueue = this.refreshQueue.bind(this);
    this.refreshHandle = this.refreshHandle.bind(this);
    this.onMessageReceive = this.onMessageReceive.bind(this);

    //Load channel from SharedPreferences
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      this.setState({
        channel: channel,
      });
      this.refreshQueue();
      this.refreshHandle();
    });
  }

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={Color.BACKGROUND_DARK}
          style={{elevation: 0, backgroundColor: Color.BACKGROUND}}>
          <Body>
            <Title style={{marginLeft: 10, color: Color.FONT}}>Home</Title>
          </Body>
          <Right />
        </Header>
        <Tabs>
          <Tab
            heading={
              <TabHeading
                style={{
                  justifyContent: 'center',
                  backgroundColor: Color.BACKGROUND,
                }}>
                {/* <Icon name="camera" /> */}
                <Text
                  style={{
                    color: Color.FONT,
                  }}>
                  Queue {this.state.appState}
                </Text>
                <MyBadge value={this.state.enqueue_list.length} />
              </TabHeading>
            }>
            <SockJsClient
              url={'https://channel.rooc.egeroo.com/rooc'}
              headers={{
                Authorization: 'Bearer ' + this.state.channel.userchnltoken,
              }}
              // topics={['/monitor/newqueue', '/monitor/chat/room/48733']}
              topics={[...this.state.topics, ...this.state.id_messages]}
              onMessage={this.onMessageReceive}
              onConnect={() => {
                console.warn("WOW! it's connected");
                this.setState({clientConnected: true});
              }}
              onDisconnect={() => {
                console.warn('Server HomePage : disconnected');
                this.setState({clientConnected: false});
              }}
              autoReconnect={true}
              getRetryInterval={() => {
                return 3000;
              }}
              debug={false}
              ref={client => {
                this.clientRef = client;
              }}
            />
            <TabChats
              navigation={this.props.navigation}
              channel={this.state.channel}
              list_contact={this.state.enqueue_list}
              onPress={value => {
                if (value != null) {
                  EnqueueApi.requestHandle(value.id, response => {
                    this.refreshQueue();
                    this.refreshHandle();
                    this.props.selectChat(value);
                    this.props.navigation.navigate('Chatroom', {
                      chat: value,
                    });
                  });
                }
              }}
            />
          </Tab>
          <Tab
            heading={
              <TabHeading
                style={{
                  justifyContent: 'center',
                  backgroundColor: Color.BACKGROUND,
                }}>
                <Text
                  style={{
                    color: Color.FONT,
                  }}>
                  Handle
                </Text>
                <MyBadge value={this.state.handle_list.length} />
              </TabHeading>
            }>
            <TabChats
              navigation={this.props.navigation}
              channel={this.state.channel}
              list_contact={this.state.handle_list}
              onPress={value => {
                this.props.selectChat(value);
                this.props.navigation.navigate('Chatroom', {
                  chat: value,
                });
              }}
            />
          </Tab>
        </Tabs>
      </Container>
    );
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({appState: nextAppState});
  };

  refreshQueue() {
    EnqueueApi.getEnqueueList(
      {
        limit: 10,
        lastchattime: '0',
      },
      response => {
        if (response.success) {
          this.setState({
            enqueue_list: response.data,
          });
          // && this.state.appState != 'active'
          if (response.data.length > 0 && this.state.appState != 'active') {
            PushNotification.localNotification({
              title: 'RooC Notification', // (optional)
              message: 'Terdapat ' + response.data.length + ' Queue baru', // (required)
            });
          }
        }
      },
    );
  }

  refreshHandle() {
    HandleApi.getHandleList(response => {
      if (response.success) {
        this.setState({
          id_messages: response.data.map(value => {
            return '/monitor/chat/room/' + value.id;
          }),
        });
        this.setState({
          handle_list: response.data,
        });
        if (
          this.state.id_new_message != null &&
          this.state.appState != 'active'
        ) {
          var contact = this.state.handle_list.find(
            value => value.id == this.state.id_new_message,
          );
          PushNotification.localNotification({
            title: 'RooC Notification', // (optional)
            message: contact.initialuser.name + ' : ' + contact.lastmessage, // (required)
          });
          this.setState({
            id_new_message: null,
          });
        }
      }
    });
  }

  onMessageReceive(msg, topic) {
    console.log('__SOKCET', topic);
    if (topic.search('/monitor/chat/room/') !== -1) {
      this.setState({
        id_new_message: topic.substring(19, topic.length),
      });
      this.refreshHandle();
    } else if (topic === STOMP.NEW_QUEUE) {
      this.refreshQueue();
    }
  }
}

//======= REDUX ====== //
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectChat,
    },
    dispatch,
  );
}

function mapStateToProps(state) {
  const {selected_chat} = state;
  return {selected_chat};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
