import React from 'react';
import {View} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Badge,
} from 'native-base';
import MyBadge from '../../components/MyBadge';
import Contact from '../../components/Contact';
import EnqueueApi from '../../data/network/EnqueueApi';
import {getImageFromRoomType} from '../../tools/ImageTools';

//redux
import {selectChat} from '../../data/redux/actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class TabChats extends React.Component {
  constructor(props) {
    super(props);
    // console.warn('TabChats first props : ', this.props);
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <Content>
        {this.props.list_contact == null ||
        this.props.list_contact.length == 0 ? (
          <Text style={{margin: 10, color: 'gray'}}>Tidak ada data</Text>
        ) : (
          this.props.list_contact.map(value => {
            return (
              <Contact
                thumbnail={getImageFromRoomType(value.roomtype)}
                name={value.subject}
                message={value.lastmessage}
                notifications={value.notifications}
                time={value.time}
                eventPress={() => this.props.onPress(value)}
                eventLongPress={() =>
                  this.props.onLongPress == null
                    ? alert('Event long press not set yet')
                    : this.props.onLongPress(value)
                }
              />
            );
          })
        )}
      </Content>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectChat,
    },
    dispatch,
  );
}

function mapStateToProps(state) {
  const {selected_chat} = state;
  return {selected_chat};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TabChats);
