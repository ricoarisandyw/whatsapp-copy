import React, {Component} from 'react';
import {View, Text, Alert, Image, ImageBackground} from 'react-native';
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Icon,
  Button,
  Toast,
} from 'native-base';
import UserApi from '../../data/network/UserApi';
import MyPref from '../../tools/MyPref';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {saveLogin} from '../../data/redux/actions/index';
import ChatroomApi from '../../data/network/ChatroomApi';

import {Color} from '../../tools/MyStyle';

class LoginScreen extends Component {
  state = {
    username: null,
    password: null,
    onLoading: false,
  };

  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    //Check wehter already login or not, if already then move it directly into Home
    MyPref.getItem('is_login', value => {
      console.log('Have you logged in ?', value);
      if (value != null && value === true) {
        MyPref.getItem('user', user => {
          this.setState({
            username: user.username,
            password: user.password,
          });
          this.login();
        });
      }
    });

    // this.props.navigation.goBack(null);
  }

  componentDidMount() {
    // //Check wehter already login or not, if already then move it directly into Home
    // MyPref.getItem('is_login', value => {
    //   console.log('Have you logged in ?', value);
    //   if (value != null && value === true) {
    //     MyPref.getItem('user', user => {
    //       this.setState({
    //         username: user.username,
    //         password: user.password,
    //       });
    //       this.login();
    //     });
    //   }
    // });
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={require('../../assets/image/bg_login.png')}
          style={{height: '100%', width: '100%'}}>
          <Content style={{margin: 30}}>
            <Image
              style={this.loginStyle.logo}
              source={require('../../assets/image/via.png')}
              resizeMode="contain"
            />
            <Image
              style={this.loginStyle.logo_text}
              source={require('../../assets/image/logo_rooc.png')}
              resizeMode="contain"
            />
            <Form>
              <Item regular style={this.loginStyle.form}>
                <Icon active name="person" />
                <Input
                  placeholder="Username"
                  onChangeText={text => {
                    this.setState({username: text});
                  }}
                />
              </Item>
              <Item regular style={this.loginStyle.form}>
                <Icon active name="lock" />
                <Input
                  secureTextEntry={true}
                  placeholder="Password"
                  onChangeText={text => {
                    this.setState({password: text});
                  }}
                />
              </Item>
              <Button
                full
                onPress={this.login}
                style={{
                  marginTop: 10,
                  backgroundColor: Color.BUBBLE_GREEN,
                  borderBottomWidth: 2,
                  borderBottomColor: '#b8cfa7',
                }}>
                <Text style={this.loginStyle.btn_login}>
                  {this.state.onLoading ? 'LOADING . . .' : 'LOGIN'}
                </Text>
              </Button>
              <View>
                <Text
                  style={{textAlign: 'center', marginTop: 20, color: 'gray'}}>
                  Rooc Admin v1.0.
                </Text>
              </View>
            </Form>
          </Content>
        </ImageBackground>
      </Container>
    );
  }

  login() {
    console.log('onLoading', this.state.onLoading);
    this.setState({onLoading: true});
    UserApi.login(
      {
        username: this.state.username,
        password: this.state.password,
      },
      response => {
        if (response.success) {
          if (response.data.access_token != null) {
            this.props.saveLogin({
              username: this.state.username,
              password: this.state.password,
              user: {
                username: this.state.username,
                password: this.state.password,
                access_token: response.data.access_token,
              },
            });
            this.askChannel(response.data.access_token);
          } else {
            Toast.show({
              text: 'Username atau Password Salah!',
              buttonText: 'Okay',
              type: 'danger',
            });
            this.setState({onLoading: false});
          }
        } else {
          alert('System error');
          this.setState({onLoading: false});
        }
      },
    );
    return true;
  }

  askChannel(token) {
    console.log('ask token');
    const {navigate} = this.props.navigation;
    UserApi.requestChannel(token, response => {
      if (response.success) {
        MyPref.setItem('channel', response.data);
        this.props.saveLogin({
          channel: response.data,
        });
        navigate('Home', response.data);
      }
      this.setState({onLoading: false});
    });
  }

  loginStyle = {
    form: {
      marginTop: 10,
      marginBottom: 10,
      backgroundColor: 'white',
    },
    btn_login: {
      fontWeight: 'bold',
    },
    logo: {
      alignSelf: 'center',
      height: 200,
    },
    logo_text: {
      alignSelf: 'center',
      height: 100,
    },
  };
}

// export default connect()(FormExample);
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      saveLogin,
    },
    dispatch,
  );
}

const mapStateToProps = state => {
  const {channel, user, username, password} = state;
  return {
    channel,
    user,
    username,
    password,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
