export const getImageFromRoomType = roomtype => {
  if (roomtype === 'facebook')
    return 'https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Facebook_Messenger-512.png';
  if (roomtype === 'line')
    return 'https://seeklogo.com/images/L/line-messenger-logo-F9E667D0A2-seeklogo.com.png';
  if (roomtype === 'rooc') return 'https://egeroo.com/via/via.jpeg';
  if (roomtype === 'whatsapp')
    return 'https://i2.wp.com/www.androidiospack.com/wp-content/uploads/2017/07/whats.png?fit=1024%2C727&ssl=1';
  if (roomtype === 'telegram')
    return 'https://seeklogo.com/images/T/telegram-logo-AD3D08A014-seeklogo.com.png';
  return 'https://images.vexels.com/media/users/3/152864/isolated/preview/2e095de08301a57890aad6898ad8ba4c-yellow-circle-question-mark-icon-by-vexels.png';
};
