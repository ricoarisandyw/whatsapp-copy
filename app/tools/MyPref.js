import SharedPreferences from 'react-native-shared-preferences';

export default class MyPref {
  static project_packages = 'juragan.egeroo.ai';

  static setItem = async (key, value) => {
    SharedPreferences.setName(this.project_packages);
    SharedPreferences.setItem(key, JSON.stringify(value));
  };

  static getItem = async (key, fx) => {
    SharedPreferences.setName(this.project_packages);
    SharedPreferences.getItem(key, value => {
      fx(JSON.parse(value));
    });
  };

  //Static Variable
  static getChannel = fx => {
    this.getItem('channel', fx);
  };
}
