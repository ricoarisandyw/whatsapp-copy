import {StyleSheet} from 'react-native';

/*
    Helper for styling in react native.
    ? How to Use :
    <Text style={merge(s.a,s.b,s.c)}
    We can use any of this using our own style:
    const $btn_primary = merge(s.p10,s.round(20),s.shadow(10));
*/
export const Color = {
  FONT: '#6B6F74',
  BACKGROUND: '#EDEDED',
  BACKGROUND_ACCENT: '#DDDDDD',
  BACKGROUND_DARK: '#6b6f74',
  BUBBLE_GREEN: '#DCF8C6',
  BUBBLE_BLUE: '#4BCFFA',
};

const S = {
  lorem:
    'In alteration insipidity impression by travelling reasonable up motionless. Of regard warmth by unable sudden garden ladies. No kept hung am size spot no. Likewise led and dissuade rejoiced welcomed husbands boy. Do listening on he suspected resembled. Water would still if to. Position boy required law moderate was may. ',
  h1: {
    fontSize: 24,
  },
  h2: {
    fontSize: 20,
  },
  h3: {
    fontSize: 18,
  },
  b: {
    fontSize: 9,
  },
  t_white: {
    color: 'white',
  },
  t_color: color => {
    return {
      color: color,
    };
  },
  bg_color: color => {
    return {
      backgroundColor: color,
    };
  },
  p: param => {
    return {
      padding: param,
    };
  },
  radius: value => {
    return {
      borderRadius: value == null ? 0 : value,
    };
  },
  shadow: value => {
    return {
      borderRadius: value == null ? 0 : value,
    };
  },
  merge: arr => {
    return StyleSheet.flatten([...arr]);
  },
  build: list_style_from_string => {
    var arr = list_style_from_string.split(' ');
    var extracted_style = [];
    var it = S;
    extracted_style = arr.map(val => {
      //   console.log(it[val]);
      if (it[val] == null) {
        if (val.substring(0, 1) == 'p') {
          return it.p(parseInt(val.substring(1, val.legnth)));
        }
      }
      return it[val];
    });
    var flatten = StyleSheet.flatten([extracted_style]);
    return flatten;
  },
};
export default S;
