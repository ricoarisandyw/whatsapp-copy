import BaseApi from './BaseApi';
import MyPref from '../../tools/MyPref';

export default class ChatroomApi {
  // 3. Request Onqueue

  // Base URL : <<CHANNEL SERVER>>
  // Path : /monitor/onqueuelimit?limit=<<LIMIT>>&lastchattime=<<LAST CHAT TIME>>
  // Method: GET
  // Header:
  // - Content-Type 	: application/json
  // - Authorization 	: Bearer <<CHANNEL TOKEN>>

  // Response:
  // //LIST
  static openChatroom({id, limit, endchattime}, callback) {
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      var full_url = channel.channelip + '/chat/roomparam?';
      if (id != null) full_url = full_url + '&id=' + id;

      if (limit != null) full_url = full_url + '&limit=' + limit;

      if (endchattime != null)
        full_url = full_url + '&lastchattime=' + endchattime;

      var header = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + channel.userchnltoken,
      };

      console.log('__HEADER_SEND_MESSAG__');
      console.log(JSON.stringify(header));

      fetch(full_url, {
        method: 'GET',
        headers: header,
      })
        .then(response => response.text())
        .then(response => {
          console.log('OpenChat Room : ', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.log('Error OpenChatRoom', error);
        });
    });
  }

  static sendCustomChat = ({body, id_room, source, rooc_token}, callback) => {
    var request = {
      room: id_room,
      messagetype: body.messagetype,
      message: body.message || '',
      time: Date.now().toString(),
      status: 'pending',
      senderasbot: true,
      originid: Date.now().toString(),
      info: body.info,
      data: body.raw.body,
    };

    if (source != null) {
      request = {
        ...request,
        source: source,
      };
    }

    console.log('__SEND CHAT___');
    console.log('__REQUEST__ :' + JSON.stringify(request));

    MyPref.getItem('channel', json => {
      const channel = JSON.parse(json);

      var header = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + channel.userchnltoken,
      };

      console.log('__HEADER__' + JSON.stringify(header));

      fetch(channel.channelip + '/chat/sendmessage', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({
          chat: request,
        }),
      })
        .then(response => response.text())
        .then(response => {
          console.log('Send Custom Message Room : ', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.log('Error Send Custom Message', error);
          callback(BaseApi.Response(false, JSON.parse(error)));
        });
    });
  };

  static sendChat = ({text, id_room}, callback) => {
    MyPref.getItem('channel', json => {
      const channel = JSON.parse(json);

      fetch(channel.channelip + '/chat/sendmessage', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
        body: JSON.stringify({
          chat: {
            room: id_room,
            messagetype: 'text',
            message: text,
            time: Date.now().toString(),
            status: 'pending',
            senderasbot: true,
            originid: Date.now().toString(),
            // chatref: 46089,
          },
        }),
      })
        .then(response => response.text())
        .then(response => {
          console.log('Send Message Room : ', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.log('Error Send Message', error);
          callback(BaseApi.Response(false, JSON.parse(error)));
        });
    });
  };

  static leaveRoom = (id, callback) => {
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      console.warn(channel.channelip + '/room/leave/' + id);
      fetch(channel.channelip + '/room/leave/' + id, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
      })
        .then(response => response.text())
        .then(response => {
          console.log('Leave Room : ', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.log('Error Send Message', error);
          callback(BaseApi.Response(false, JSON.parse(error)));
        });
    });
  };
}
