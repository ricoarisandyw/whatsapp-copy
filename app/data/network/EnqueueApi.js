import BaseApi from './BaseApi';
import MyPref from '../../tools/MyPref';

export default class EnqueueApi {
  // 3. Request Onqueue

  // Base URL : <<CHANNEL SERVER>>
  // Path : /monitor/onqueuelimit?limit=<<LIMIT>>&lastchattime=<<LAST CHAT TIME>>
  // Method: GET
  // Header:
  // - Content-Type 	: application/json
  // - Authorization 	: Bearer <<CHANNEL TOKEN>>

  // Response:
  // //LIST
  static getEnqueueList({limit, lastchattime}, callback) {
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      var full_url = channel.channelip + '/monitor/onqueuelimit?';
      if (limit != null) full_url = full_url + '&limit=' + limit;

      if (lastchattime != null)
        full_url = full_url + '&lastchattime=' + lastchattime;

      fetch(full_url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
      })
        .then(response => response.text())
        .then(response => {
          console.log('Get Enqueue', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.log('Error GET Enqueue', error);
          callback(BaseApi.response(false, null));
        });
    });
  }

  /**
   * Move Enqueue to Handle
   * @param {string} idRoom - id room in enqueue
   * @param {function} callback - callback function
   */
  static requestHandle(id_room, callback) {
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);

      fetch(channel.channelip + '/room/handle/' + id_room, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
      })
        .then(response => response.text())
        .then(response => {
          console.log('request Handle', response);
          callback(BaseApi.Response(true, JSON.parse(response)));
        })
        .catch(error => {
          console.error('Error request Handle', error);
        });
    });
  }
}
