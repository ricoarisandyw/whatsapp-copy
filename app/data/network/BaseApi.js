export default class BaseApi {
  static BASE_URL = 'https://api.rooc.egeroo.com/rooc';
  static COMPOSER_URL = 'https://api.rooc.egeroo.com/composer';
  static BASE_TENANT = 'default';
  // <<Username>> = admin@egeroo.ai
  // <<Password>> = Admin@123
  static Response = (success, data) => {
    return {
      data: data,
      success: success,
    };
  };
}
