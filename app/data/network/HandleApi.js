import BaseApi from './BaseApi';
import MyPref from '../../tools/MyPref';

export default class HandleApi {
  //fetching data handle
  static getHandleList(callback) {
    MyPref.getItem('channel', channel_json => {
      const channel = JSON.parse(channel_json);
      fetch(channel.channelip + '/monitor/onhandle', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
      })
        .then(response => response.text())
        .then(response => {
          console.log('Get Handle Api', JSON.parse(response)[0]);
          if (response) {
            callback(BaseApi.Response(true, JSON.parse(response)));
          }
        })
        .catch(error => {
          callback(BaseApi.response(false, error));
        });
    });
  }
}
