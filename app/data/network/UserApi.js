import BaseApi from './BaseApi';
import MyPref from '../../tools/MyPref';

export default class UserApi {
  static clearUserData = () => {
    MyPref.setItem('is_login', false);
  };

  static login(data, callback) {
    console.log('LOGIN API');
    fetch(BaseApi.BASE_URL + '/login/dologin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        tenantID: BaseApi.BASE_TENANT,
      },
      body: JSON.stringify(data),
    })
      .then(response => {
        return response.text();
      })
      .then(response => {
        console.log(response);

        //SAVE RESPONSE
        MyPref.setItem('user', {
          username: data.username,
          password: data.password,
        });
        MyPref.setItem('is_login', true);
        MyPref.setItem('access_token', response.access_token);

        if (callback != null) {
          callback(BaseApi.Response(true, JSON.parse(response)));
        }
      })
      .catch(error => {
        console.error(error);
        callback(BaseApi.Response(false, JSON.parse(error)));
      });
  }

  //   2. Request Channel Server & Token

  // Base URL : <<ROOC SERVER>>
  // Path : /login/dochanneltoken
  // Method: GET
  // Header:
  // - Content-Type 	: application/json
  // - tenantID 		: <<TENANT>>
  // - Authorization 	: <<ROOC TOKEN>>

  // Response:
  // {
  //   "channelip": <<CHANNEL SERVER>>
  //   ,"userchnltoken": <<CHANNEL TOKEN>>
  //   ,"isValid": true
  // }
  static requestChannel(token, callback) {
    fetch(BaseApi.BASE_URL + '/login/dochanneltoken', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        tenantID: BaseApi.BASE_TENANT,
        Authorization: token,
      },
    })
      .then(response => response.text())
      .then(response => {
        console.log('Request Channel', response);
        callback(BaseApi.Response(true, response));
      })
      .catch(error => {
        console.error(error);
      });
  }
}
