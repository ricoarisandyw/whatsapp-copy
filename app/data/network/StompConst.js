export const STOMP = {
  NEW_QUEUE: '/monitor/newqueue',
  CHAT_ROOM: '/monitor/chat/room/',
  ROOM: '/monitor/room/',
};
