import BaseApi from './BaseApi';
import MyPref from '../../tools/MyPref';

export default class SuggestionApi {
  static getSuggestionById = (id, callback) => {
    var channel;
    MyPref.getItem('channel', channel_json => {
      channel = JSON.parse(channel_json);
      fetch(channel.channelip + '/chat/answer/' + id, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + channel.userchnltoken,
        },
      })
        .then(response => response.json())
        .then(json => {
          callback(json);
        });
    });
  };

  static getAnswerBySuggestion = ({suggestion, rooc_token}, callback) => {
    var channel;
    var request = {
      faqId: suggestion.faqid,
      id: suggestion.response.replace('_ROOC2', ''),
      question: suggestion.question,
      realChat: suggestion.realChat,
      data: suggestion.data,
    };
    console.warn('__REQUEST__ : ' + JSON.stringify(request));
    console.warn('__TOKENT__ : ' + JSON.stringify(rooc_token));
    MyPref.getItem('channel', channel_json => {
      channel = JSON.parse(channel_json);
      fetch(BaseApi.COMPOSER_URL + '/compose', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: 'Bearer ' + channel.userchnltoken,
          Authorization: rooc_token,
          tenantID: BaseApi.BASE_TENANT,
        },
        body: JSON.stringify(request),
      })
        .then(response => response.json())
        .then(json => {
          console.log('Response Answer', json);
          callback(json);
        });
    });
  };

  static getKnowledge(callback) {
    MyPref.getItem('access_token', access_token => {
      fetch(BaseApi.BASE_URL + '/directory/listdirectoryintent', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          tenantID: BaseApi.BASE_TENANT,
          Authorization: access_token,
        },
      })
        .then(response => response.json())
        .then(json => {
          console.log('Response Answer', json);
          callback(json);
        });
    });
  }
}
