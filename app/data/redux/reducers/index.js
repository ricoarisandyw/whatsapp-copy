//* REDUCERS CREATE STATE FOR YOUT APPLICATION

import {
  ADD_ARTICLE,
  LOGIN,
  SELECT_CHAT,
  SELECT_BUBBLE,
  LOAD_CHAT,
  LOAD_ENQUEUE,
  LOAD_HANDLE,
  LOAD_KNOWLEDGE,
  LOAD_SUGGESTION,
  SELECT_SUGGESTION,
  SELECT_KNOWLEDGE,
  SAVE_LOGIN_RESPONSE,
} from '../constants/action-types';

//* it will be used in whole application
const initialState = {
  articles: [],
  remoteArticles: [],
  dummy: [],
  //Login Page
  channel: {},
  user: {},
  username: '',
  password: '',
  //Home Page
  selected_chat: {},
  enqueue_list: [],
  handle_list: [],
  //Chat Page
  chats: [],
  selected_bubble: {},
  //Suggestion Page
  suggestions: [],
  knowledges: [],
  selected_suggestion: [],
  selected_knowledge: [],
};

//* receive signal/action which can change initialState above
function rootReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_ARTICLE: {
      return Object.assign({}, state, {
        articles: state.articles.concat(action.payload),
      });
    }

    //Login
    case SAVE_LOGIN_RESPONSE: {
      return Object.assign({}, state, {
        channel: action.payload.channel || state.channel,
        username: action.payload.username || state.username,
        password: action.payload.password || state.password,
        user: action.payload.user || state.user,
      });
    }

    //HomePage
    case SELECT_CHAT: {
      return Object.assign({}, state, {
        selected_chat: action.payload,
      });
    }
    case LOAD_ENQUEUE: {
      return Object.assign({}, state, {
        enqueue_list: action.payload,
      });
    }
    case LOAD_HANDLE: {
      return Object.assign({}, state, {
        handle_list: action.payload,
      });
    }

    //ChatPage
    case LOAD_CHAT: {
      return Object.assign({}, state, {
        chats: action.payload,
      });
    }
    case SELECT_BUBBLE: {
      return Object.assign({}, state, {
        selected_bubble: action.payload,
      });
    }

    //Suggestion
    case LOAD_KNOWLEDGE: {
      return Object.assign({}, state, {
        knowledges: action.payload,
      });
    }
    case LOAD_SUGGESTION: {
      return Object.assign({}, state, {
        suggestions: action.payload,
      });
    }
    case SELECT_SUGGESTION: {
      return Object.assign({}, state, {
        selected_suggestion: action.payload,
      });
    }
    case SELECT_KNOWLEDGE: {
      return Object.assign({}, state, {
        selected_knowledge: action.payload,
      });
    }
  }

  if (action.type === 'DUMMY') {
    alert('DUMMY!', JSON.stringify(state));
    return Object.assign({}, state, {
      dummy: state.dummy.concat(state.dummy.length),
    });
  }

  if (action.type === 'FOUND_BAD_WORD') {
    alert('FOUND BAD WORD');
  }

  //* used with thunk for asynchronous request
  if (action.type === 'DATA_LOADED') {
    return Object.assign({}, state, {
      remoteArticles: state.remoteArticles.concat(action.payload),
    });
  }
  return state;
}

export default rootReducer;
