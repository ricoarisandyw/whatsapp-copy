//* ALL REDUX ACTIONS ARE STORED HEERE

import {
  ADD_ARTICLE,
  SELECT_CHAT,
  SAVE_LOGIN_RESPONSE,
  SELECT_BUBBLE,
  SELECT_KNOWLEDGE,
  SELECT_SUGGESTION,
  LOAD_CHAT,
  LOAD_ENQUEUE,
  LOAD_HANDLE,
  LOAD_KNOWLEDGE,
  LOAD_SUGGESTION,
} from '../constants/action-types';
import UserApi from '../../network/UserApi';

export function addArticle(payload) {
  return {type: ADD_ARTICLE, payload};
}

export function selectChat(payload) {
  return {type: SELECT_CHAT, payload};
}

export const addDummy = payload => {
  type: 'DUMMY', payload;
};

export function saveLogin(payload) {
  return {type: SAVE_LOGIN_RESPONSE, payload};
}

export function selectBubble(payload) {
  return {type: SELECT_BUBBLE, payload};
}

export function selectKnowledge(payload) {
  return {type: SELECT_KNOWLEDGE, payload};
}

export function selectSuggestion(payload) {
  return {type: SELECT_SUGGESTION, payload};
}

export function loadSuggestion() {
  return {type: LOAD_SUGGESTION};
}

export function loadKnowledge() {
  return {type: LOAD_KNOWLEDGE};
}

export function getData() {
  return function(dispatch) {
    return fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(json => {
        console.log('RESPONSE getData', json);
        dispatch({type: 'DATA_LOADED', payload: json});
      });
  };
}
