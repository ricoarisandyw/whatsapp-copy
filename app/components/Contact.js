import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import MyBadge from './MyBadge';

//redux
import {selectChat} from '../data/redux/actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

export default class Contact extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        style={style.contactBox}
        onPress={() => {
          this.props.eventPress();
        }}
        onLongPress={() => {
          this.props.eventLongPress();
        }}>
        <Image
          style={style.thumbnail}
          source={{
            uri: this.props.thumbnail,
          }}
        />
        <View style={style.contactDetail}>
          <Text style={style.bold}>{this.props.name}</Text>
          <Text style={{color: 'gray'}} numberOfLines={2}>
            {this.props.message}
          </Text>
        </View>
        <View style={style.contactInfo}>
          <Text style={{fontSize: 12, color: 'gray'}}>{this.props.time}</Text>
          <MyBadge value={this.props.notifications} />
        </View>
      </TouchableOpacity>
    );
  }
}

const style = {
  contactInfo: {
    width: 70,
    height: 50,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  contactBox: {
    flexDirection: 'row',
    padding: 15,
    borderBottomWidth: 0.5,
    borderColor: 'silver',
    alignContent: 'auto',
  },
  contactDetail: {
    flex: 1,
    marginLeft: 20,
    flexDirection: 'column',
  },
  bold: {
    fontSize: 16,
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
};
