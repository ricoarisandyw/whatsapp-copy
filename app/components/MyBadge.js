import React from 'react';
import {View, Text} from 'react-native';
import S from '../tools/MyStyle';

const MyBadge = props => {
  if (props.value == null || parseInt(props.value) == 0) {
    return <View />;
  } else {
    return (
      <View style={[badgeStyle]}>
        <Text style={[textStyle]}>
          {parseInt(props.value) > 99 ? '99+' : props.value}
        </Text>
      </View>
    );
  }
};

const badgeStyle = {
  height: 25,
  width: 25,
  borderRadius: 100,
  backgroundColor: '#e74c3c',
  justifyContent: 'center',
  alignItems: 'center',
};

const textStyle = {color: 'white', fontSize: 12};

export default MyBadge;
