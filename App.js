/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import HomePage from './app/page/home/HomePage';
import ChatPage from './app/page/chats/ChatPage';
import LoginScreen from './app/page/login/LoginScreen';
import SplashScreen from './app/page/splash/SplashScreen';
import SuggestionScreen from './app/page/suggestion/SuggestionScreen';
import VideoScreen from './app/page/video/VideoScreen';
import {Root} from 'native-base';
import store from './app/data/redux/store/index';
import {Provider} from 'react-redux';

const MainNavigator = createStackNavigator(
  {
    Home: {
      screen: HomePage,
      navigationOptions: {
        headerShown: false,
      },
    },
    Chatroom: {
      screen: ChatPage,
      navigationOptions: {
        headerShown: false,
        headerTintColor: 'yellow',
      },
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    Suggestion: {
      screen: SuggestionScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    Video: {
      screen: VideoScreen,
    },
  },
  {
    initialRouteName: 'Splash',
  },
);

const App = createAppContainer(MainNavigator);

// export default App;
export default () => (
  <Root>
    <Provider store={store}>
      <App />
    </Provider>
  </Root>
);
